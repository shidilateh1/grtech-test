<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
         // Using closure based composers...
        View::composer('layouts.app', function ($view) {

            $menuList = [
                [
                    "title"=>"Dashboard",
                    "path"=>"dashboard",
                    "slug"=>null
                ],
                [
                    "title"=>"Companies",
                    "path"=>"companies",
                    "slug"=>"admin"
                ],
                [
                    "title"=>"Employees",
                    "path"=>"employees",
                    "slug"=>"admin"
                ]
            ];

            $view->with('menuList', $menuList);
        });
    }
}
