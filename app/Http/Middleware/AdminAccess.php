<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Role;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->user()->roles->contains(Role::where('slug','admin')->first())){
            // $request->session()->flash('msg', 'unauthorized access');
            // return redirect()->back();
            return abort(401);
        }

        return $next($request);
    }
}
