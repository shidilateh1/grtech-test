<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(request()->all());
        return [
            'name' => 'required',
            'email' => 'nullable|email|unique:companies,email,'.request()->route('company'),
            'logo' => 'nullable|file',
            'website' => 'nullable|url'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        // return $validator->errors();
        // return new JsonResponse(['error' => $validator->errors()], 200);
        $response = new JsonResponse(["status"=>0,"errors"=>$validator->errors()], 200);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
