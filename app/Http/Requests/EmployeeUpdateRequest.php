<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required|exists:companies,id',
            'email' => 'nullable|email|unique:employees,email,'.request()->route('employee'),
            'phone' => 'nullable',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        // return $validator->errors();
        // return new JsonResponse(['error' => $validator->errors()], 200);
        $response = new JsonResponse(["status"=>0,"errors"=>$validator->errors()], 200);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
