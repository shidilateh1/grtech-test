<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Models\Company;
use Yajra\Datatables\Datatables;
use Storage;

class CompaniesController extends Controller
{
    public function __construct(){

    }


	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getDatatableIndex()
	{
	    return Datatables::of(Company::query())->make(true);
	}

    public function index(){
    	return view('companies.index');
    }

    public function show(){
    	return view('companies.show');
    }

    public function create(){
    	return view('companies.create');
    }

    public function store(CompanyCreateRequest $request){

    	$company = Company::create($request->only(['name','email','website']));
    	if($company){
    		if ($request->hasFile('logo')) {
			    if ($request->file('logo')->isValid()) {
		
					$path = $request->file('logo')->storePublicly('logo', 'public');

				   	$company->logo = $path;
                    $company->update();
				}
			}
    		

            $request->session()->flash('success','Success add company!');
            return redirect()->route('companies');
    	}

        $request->session()->flash('msg','failed add company!');
        return redirect()->back()->withInput();
        
    }

    public function edit($id){
    	$company = Company::find($id);
    	return view('companies.partials.form-create-edit')
            ->with('type','PUT')
            ->with('action',route('companies.update',$id))
            ->with('button','Update')
            ->with('company',$company)->render();
    }

    public function update(CompanyUpdateRequest $request,$id){
        
    	$company = Company::find($id);
    	if($company->update($request->only(['name','email','website']))){
    		if ($request->hasFile('logo')) {
			    if ($request->file('logo')->isValid()) {
		          try {
                    Storage::disk('public')->delete($company->logo);
                    $path = $request->file('logo')->storePublicly('logo', 'public');

                    $company->logo = $path;
                    $company->update();       
                  } catch (Exception $e) {
                      
                  }
					
				}
			}
    		
            $request->session()->flash('success','Success update company!');
            return response()->json(["status"=>1]);
    	}


        $request->session()->flash('msg','failed update company!');
        return response()->json(["status"=>2,"view"=>view('companies.partials.form-create-edit')->withInput()->render()]);
        
    }

    public function destroy(Request $request,$id){
        $company = Company::find($id);
        if($company){
            $pathLogo = $company->logo;
            if($company->delete()){
                
                Storage::disk('public')->delete($pathLogo);
                $request->session()->flash('success','Success delete company!');
                return redirect()->route('companies');
            }
        }
        


        $request->session()->flash('msg','failed delete company!');
        return redirect()->back();
    }

}
