<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmployeeCreateRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Employee;
use App\Models\Company;
use Yajra\Datatables\Datatables;

class EmployeesController extends Controller
{
    public function __construct(){

    }

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getDatatableIndex()
	{
		$employees = Employee::with('company')->get();

	    return Datatables::of($employees)->make(true);
	}

    public function index(){
    	return view('employees.index');
    }

    public function show(){
    	return view('employees.show');
    }

    public function create(){
    	$companies = Company::pluck('name','id');

    	// dd($companies);
    	return view('employees.create')->with('companies',$companies);
    }

    public function store(EmployeeCreateRequest $request){

    	$employee = Employee::create($request->only(['first_name','last_name','company_id','email','phone']));
    	if($employee){

            $request->session()->flash('success','Success add employee!');
            return redirect()->route('employees');
    	}

        $request->session()->flash('msg','failed add employee!');
        return redirect()->back()->withInput();
        
    }

    public function edit(Request $request,$id){

    	$companies = Company::pluck('name','id');
    	$employee = Employee::find($id);

        return view('employees.partials.form-create-edit')
            ->with('type','PUT')
            ->with('action',route('employees.update',$id))
            ->with('button','Update')
            ->with('companies',$companies)
            ->with('employee',$employee)->render();
    }

    public function update(EmployeeUpdateRequest $request,$id){
        // $validatedData = $request->validated();
        // dd($validatedData);
    	$employee = Employee::find($id);
    	if($employee->update($request->only(['first_name','last_name','company_id','email','phone']))){
    		
            $request->session()->flash('success','Success update employee!');
            return response()->json(["status"=>1]);
    	}


        $request->session()->flash('msg','failed update employee!');
        return response()->json(["status"=>2,"view"=>view('employees.partials.form-create-edit')->withInput()->render()]);
    }

    public function destroy(Request $request,$id){
    	$employee = Employee::find($id);
    	if($employee->delete()){
    		
            $request->session()->flash('success','Success delete employee!');
            return redirect()->route('employees');
    	}


        $request->session()->flash('msg','failed delete employee!');
        return redirect()->back();
    }
}
