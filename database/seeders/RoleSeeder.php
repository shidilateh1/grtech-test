<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use Carbon\Carbon;
use DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = collect([
    		[
	            'name' => "Administrator",
	            'slug' => 'admin',
	        ],
    		[
	            'name' => "User",
	            'slug' => 'user',
	        ]
    	]);

    	$roles = Role::whereIn('slug',$data->pluck('slug')->toArray())->get()->pluck('slug');

    	if($roles->count()>0)
    	{
    		$data = $data->whereNotIn('slug',$roles);
    	}

    	$data = $data->map(function($val){
    		$collection = collect($val);
    		$collection->put('created_at', Carbon::now());
    		$collection->put('updated_at', Carbon::now());
    		return $collection->toArray();
    	});

    	// dd($data);
        DB::table('roles')->insert($data->toArray());
    }
}
