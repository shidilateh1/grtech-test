<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// filter user already exist

    	$data = collect([
    		[
	            'name' => "Administrator",
	            'email' => 'admin@admin.com',
	            'password' => Hash::make('password'),
	            'slug' => 'admin'
	        ],
    		[
	            'name' => "User",
	            'email' => 'user@user.com',
	            'password' => Hash::make('password'),
	            'slug' => 'user'
	        ]
    	]);

    	$users = User::whereIn('email',$data->pluck('email')->toArray())->get()->pluck('email');

    	if($users->count()>0)
    	{
    		$data = $data->whereNotIn('email',$users);
    	}

    	$data = $data->map(function($val){
    		$collection = collect($val);
    		$collection->put('created_at', Carbon::now());
    		$collection->put('updated_at', Carbon::now());
    		return $collection->toArray();
    	});

    	$data->each(function($value,$index){
    		$user = User::create(collect($value)->only(['name','email','password','created_at','updated_at'])->toArray());

    		Role::where('slug',$value['slug'])->first()->roleUsers()->save(new RoleUser([
    			'user_id'=>$user->id
    		]));
    	});
    	// dd($data);
        
    }
}
