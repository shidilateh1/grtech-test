
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function(){

    $(document).on('change','.inputUploadImg',function(e){
    
        let files = e.target.files || e.dataTransfer.files;
        if (!files.length) {
            return;
        }

        $(document).find(".show-image").prop('src',URL.createObjectURL(files[0]));

    });

    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {
             var base = $("<form/>").addClass("general-form-delete").attr('action',$(this).attr('href')).attr('method','POST');
             var csrf_token = $(document).find("[name='csrf-token']").attr("content");

             base.append($("<input/>").attr("name","_method").attr("type","hidden").val("DELETE"));
             base.append($("<input/>").attr("name","_token").attr("type","hidden").val(csrf_token));

             $(document).find("body").append(base);
             $(document).find("body .general-form-delete").submit();
         }
     });

        
    });

    $(document).on('click','.btn-edit-u',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        var id = $(this).attr('data-id');

        console.log("edit",url);

        window.axios.get(url)
          .then(function (response) {
            $("#"+id).find(".modal-body").html(response.data);
            $("#"+id).find(".modal-body form").attr("data-id",id);
            $("#"+id).modal("show");
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(function () {
                    // always executed
          });
    });
    $(document).on('click','#companyEditPopup .modal-body .btn-update-u',function(e){
        e.preventDefault();
        var parent = $(this).closest("form");
        var id = parent.attr('data-id');

        var url = parent.attr("action");

        const formData = new FormData();

        formData.append('name', parent.find('input[name=name]').val());
        formData.append('email', parent.find('input[name=email]').val());
        formData.append('website', parent.find('input[name=website]').val());
        if(parent.find('input[name=logo]')[0].files.length>0)
            formData.append('logo', parent.find('input[name=logo]')[0].files[0]);
        formData.append('_method', "PUT");
        var csrf_token = $(document).find("[name='csrf-token']").attr("content");
        formData.append('_token', csrf_token);

        let config = { headers: { 'Content-Type': 'multipart/form-data' } }

        // console.log("as",url,formData);

        // window.axios({
        //   method: "put",
        //   url: url,
        //   data: formData,
        //   {headers: { "Content-Type": "multipart/form-data" }},
        // })
        //   .then(function (response) {
        //     //handle success
        //     console.log(response);
        //   })
        //   .catch(function (response) {
        //     //handle error
        //     console.log(response);
        //   });

        //   return;
        $.ajax({
            url: url,
            data: formData,
            // cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function(data){

                if(data.status==1){

                $(document).find(".msg-alert").removeClass("show").addClass("hide");
                $("#"+id).modal("hide");
                window.location.reload();

            }
            else if(data.status==2){

                $("#"+id).find(".modal-body").html(data);

            }
            else{
                $(document).find(".msg-alert ul").html("");
                

                $.map(data.errors, function(value, index) {
                    console.log(index,value);
                    $(document).find(".msg-alert ul").append(value);
                });
                // response.data.errors.map(error=>{
                //     console.log(error);
                // });
                $(document).find(".msg-alert").removeClass("hide").addClass("show");
            }
            }
        });
        // $.put(url,formData,config).then(function(data){
        //     console.log(data);
        // });
        return;

        // formData.pipe(data => {
          window.axios.put(url, formData,undefined).then(function (response) {
            if(response.data.status==1){

                $(document).find(".msg-alert").removeClass("show").addClass("hide");
                $("#"+id).modal("hide");
                window.location.reload();

            }
            else if(response.data.status==2){

                $("#"+id).find(".modal-body").html(response.data);

            }
            else{
                $(document).find(".msg-alert ul").html("");
                

                $.map(response.data.errors, function(value, index) {
                    console.log(index,value);
                    $(document).find(".msg-alert ul").append(value);
                });
                // response.data.errors.map(error=>{
                //     console.log(error);
                // });
                $(document).find(".msg-alert").removeClass("hide").addClass("show");
            }

          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(function (error) {
            console.log(error);
                    // always executed
          });
        // });

        // window.axios.put(url,formData,config)
        //   .then(function (response) {
        //     if(response.data.status==1){

        //         $(document).find(".msg-alert").removeClass("show").addClass("hide");
        //         $("#"+id).modal("hide");
        //         window.location.reload();

        //     }
        //     else if(response.data.status==2){

        //         $("#"+id).find(".modal-body").html(response.data);

        //     }
        //     else{
        //         $(document).find(".msg-alert ul").html("");
                

        //         $.map(response.data.errors, function(value, index) {
        //             console.log(index,value);
        //             $(document).find(".msg-alert ul").append(value);
        //         });
        //         // response.data.errors.map(error=>{
        //         //     console.log(error);
        //         // });
        //         $(document).find(".msg-alert").removeClass("hide").addClass("show");
        //     }

        //   })
        //   .catch(function (error) {
        //     // handle error
        //     console.log(error);
        //   })
        //   .then(function (error) {
        //     console.log(error);
        //             // always executed
        //   });
    });

    $(document).on('click','#employeeEditPopup .modal-body .btn-update-u',function(e){
        e.preventDefault();
        var parent = $(this).closest("form");
        var id = parent.attr('data-id');

        var url = parent.attr("action");
        var formData = {
            'first_name': parent.find('input[name=first_name]').val(),
            'last_name' : parent.find('input[name=last_name]').val(),
            'phone': parent.find('input[name=phone]').val(),
            'email': parent.find('input[name=email]').val(),
            'company_id': parent.find('select[name=company_id]').val(),
            // '_method': parent.find('input[name=_method]').val(),
            '_token': parent.find('input[name=_token]').val()
        };

        console.log("as",formData);

        window.axios.put(url,formData)
          .then(function (response) {
            if(response.data.status==1){

                $(document).find(".msg-alert").removeClass("show").addClass("hide");
                $("#"+id).modal("hide");
                window.location.reload();

            }
            else if(response.data.status==2){

                $("#"+id).find(".modal-body").html(response.data);

            }

            else{
                $(document).find(".msg-alert ul").html("");
                

                $.map(response.data.errors, function(value, index) {
                    console.log(index,value);
                    $(document).find(".msg-alert ul").append(value);
                });
                // response.data.errors.map(error=>{
                //     console.log(error);
                // });
                $(document).find(".msg-alert").removeClass("hide").addClass("show");
            }

          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(function (error) {
            console.log(error);
                    // always executed
          });
    });

    var datatable1 = $('#companies-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: contextPath+"/companies/datatable-index",
        columnDefs: [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        order: [[ 1, 'asc' ]],
        columns: [
        { data: 'id' },
        { data: 'logo', render: function(data,type,row){
         var base = $("<div/>");
         var data = data ?? 'storage/default.jpg';
         return base.append($("<img/>").attr('src',contextPath+"/"+data)).html();
     } },
     { data: 'name', name: 'name' },
     { data: 'email', name: 'email' },
     { data: 'id', render: function(data,type,row){
         var base = $("<div/>");
         base.append($("<a/>").addClass('m-2 p-2').addClass('btn-edit-u').attr("data-id","companyEditPopup").attr("href",contextPath+`/companies/${data}/edit`).html("edit"));
         base.append($("<a/>").addClass('m-2 p-2').addClass('btn-delete').attr("href",contextPath+`/companies/${data}`).html("delete"));

         return base.html();
     }},
            // { data: 'created_at', render: function(data,type,row){
            // 	return moment(data).format("dddd, MMMM Do YYYY, h:mm:ss a");
            // }}
            ]
        });

    datatable1.on( 'order.dt search.dt', function () {
        datatable1.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();


    var datatable2 = $('#employees-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: contextPath+"/employees/datatable-index",
        columnDefs: [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        order: [[ 1, 'asc' ]],
        columns: [
        { data: 'id' },
        { data: 'fullname', name: 'fullname' },
        { data: 'email', name: 'email' },
        { data: 'company.name' },
        { data: 'id', render: function(data,type,row){
            var base = $("<div/>");
            base.append($("<a/>").addClass('m-2 p-2').addClass('btn-edit-u').attr("data-id","employeeEditPopup").attr("href",contextPath+`/employees/${data}/edit`).html("edit"));
            base.append($("<a/>").addClass('m-2 p-2').addClass('btn-delete').attr("href",contextPath+`/employees/${data}`).html("delete"));

            return base.html();
        }},
            // { data: 'created_at', render: function(data,type,row){
            //  return moment(data).format("dddd, MMMM Do YYYY, h:mm:ss a");
            // }}
            ]
        });

    datatable2.on( 'order.dt search.dt', function () {
        datatable2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});