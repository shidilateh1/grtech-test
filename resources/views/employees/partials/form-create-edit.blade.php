

@if(Session::has('msg'))
<div>
    <div class="bg-red-100 max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        {{Session::get('msg')}}
    </div>
</div>
@endif

<!-- @if ($errors->any())
    
@endif -->
<div class="msg-alert alert alert-danger {{$errors->any() ? 'show' : 'hide'}}">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<form enctype="multipart/form-data" method="POST" action="{{ $action }}">
    <!-- @csrf -->
    <!-- Equivalent to... -->
    <input name="_method" type="hidden" value="{{ $type }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

     <div class="form-group">
        <label class="form-label">First Name</label>
        <input type="text" value="{{ old('first_name') ?? ($employee ? $employee->first_name : null) }}" class="form-control" name="first_name">
    </div>

     <div class="form-group">
        <label class="form-label">Last Name</label>
        <input type="text" value="{{ old('last_name') ?? ($employee ? $employee->last_name : null) }}" class="form-control" name="last_name">
    </div>

    <div class="form-group">
        <label class="form-label">Company</label>
        <select name="company_id" class="form-control">
            @foreach($companies as $key => $company)
                @php $selected = ""; @endphp
                @if($employee)
                    @php $selected = $key==$employee->company_id ? 'selected' : '' @endphp
                @endif
                
                <option value="{{ $key }}" {{ $selected }}>{{$company}}</option>
            @endforeach
        </select>
        <!-- <input type="text" value="{{ old('company') ?? ($employee ? $employee->company : null) }}" class="form-control" name="company"> -->
    </div>
    
    <div class="form-group">
        <label class="form-label">Email</label>
        <input type="text" value="{{ old('email') ?? ($employee ? $employee->email : null) }}" class="form-control" name="email">
    </div>
    
    <div class="form-group">
        <label class="form-label">Phone</label>
        <input type="text" value="{{ old('phone') ?? ($employee ? $employee->phone : null) }}" class="form-control" name="phone">
    </div>
   
    
    <div class="form-group text-right">
        <input type="submit" class="btn btn-sm btn-primary btn-update-u" name="{{ $button }}"">
    </div>
</form>