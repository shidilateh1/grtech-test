<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Companies') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row">
                        <div class="col text-right mb-3">

                            <a class="btn btn-primary" href="{{ route('companies.create') }}">Add Company</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col text-right">
                            <table class="table table-bordered" id="companies-table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Logo</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div></div>

                    </div>
                </div>
            </div>
        </div>
      <!-- Modal -->
      <div class="modal fade" id="companyEditPopup" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="companyEditPopupLabel" aria-hidden="true">
          
        <!-- Vertically centered modal -->
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="companyEditPopupLabel">Edit Company</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            ...
        </div>
    </div>
</div>
</div>
</x-app-layout>