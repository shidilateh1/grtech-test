

@if(Session::has('msg'))
<div>
    <div class="bg-red-100 max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        {{Session::get('msg')}}
    </div>
</div>
@endif

<!-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->

<div class="msg-alert alert alert-danger {{$errors->any() ? 'show' : 'hide'}}">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<form enctype="multipart/form-data" method="POST" action="{{ $action }}">
    <!-- @csrf -->
    <!-- Equivalent to... -->
    <input name="_method" type="hidden" value="{{ $type }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <div class="row">
        <div class="col-12">
            <div class="img-box mb-2">
                <label class="form-label">Logo</label>
                <img class="show-image" src="/{{ $company ? $company->logo : 'storage/default.jpg' }}">
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <input type="file" class="inputUploadImg" value="{{ old('logo') ?? ($company ? $company->logo : null)  }}" name="logo">
            </div>
        </div>
    </div>
     <div class="form-group">
        <label class="form-label">Name</label>
        <input type="text" value="{{ old('name') ?? ($company ? $company->name : null) }}" class="form-control" name="name">
    </div>
    
    <div class="form-group">
        <label class="form-label">Email</label>
        <input type="text" value="{{ old('email') ?? ($company ? $company->email : null) }}" class="form-control" name="email">
    </div>
    
    <div class="form-group">
        <label class="form-label">Website</label>
        <input type="text" value="{{ old('website') ?? ($company ? $company->website : null) }}" class="form-control" name="website">
    </div>
   
   
    
    <div class="form-group text-right">
        <input type="submit" class="btn btn-sm btn-primary btn-update-u" name="{{ $button }}"">
    </div>
</form>