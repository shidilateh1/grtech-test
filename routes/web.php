<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmployeesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::middleware(['auth'])->group(function () {

	Route::get('/dashboard', function () {
	    return view('dashboard');
	})->middleware(['auth'])->name('dashboard');

	Route::get('/companies/datatable-index', 'App\Http\Controllers\CompaniesController@getDatatableIndex');
	Route::resource('/companies', CompaniesController::class)->middleware(['admin'])->name('index','companies');
	Route::get('/employees/datatable-index', 'App\Http\Controllers\EmployeesController@getDatatableIndex');
	Route::resource('/employees', EmployeesController::class)->middleware(['admin'])->name('index','employees');

});

require __DIR__.'/auth.php';
